DxpChain PHP Library
========

Overview
---
This package allows you to read from DxpChain network from selected DxpChain node. 
Package does not contain signing operations so any operation which needs signing does not work.


Installation
---
```
composer require codaone/dxpchain-php
```

Requirements
---
* PHP >= 7.0

Examples
===
DxpChain class
---
All methods are passed as rpc meh

```php
$dxpChain = new DxpChain('wss://node.com');
$block = $dxpChain->getBlock('40385973');
$dxpChain->getChainId();
```

Getting data from named api
```php
$dxpChain = new DxpChain('wss://node.com');
$block = $dxpChain->call('history', 'method_name', ['param1', 'param2']);
```


Account
---
```php
$account = new Account('account-name');
$openorders = $account->getOpenOrders();
foreach($openorders as $order) {
...
}
```

Market
---
```php
$market = new Market('DXP/USD'); // delimiter can also be : _ -
$market->getVolume24h('DXP')->getAmount();
$market->getTicker();
$market->getOrderBook(25)->getAsks();
```

Asset
---
```php
$asset = new Asset('DXP');
$asset->getId(); // 1.3.0
$asset->getPrecision(); // 5
```

General
---
Every class under Component namespace extends Object class which is iterable and has arrayAccess.
This means that for example these are possible:

```php
$market = new Market('DXP/USD');
$market->getBase()->getSymbol(); // DXP
$market['base']['symbol]; // DXP
```

```php
$account = new Account('account-name');
$account->getData('owner/weight_threshold');
$account['owner']['weight_threshold'];
$account->getBalances(); // returns balances array
$account->getData('balances/0/asset_type');
$account['balances'][0]['asset_type'];
foreach($account as $key => $value) {
...
}
```

Contributing
---
Feel free to open pull requests or add an issue

License
---
A copy of the license is available in the repository's [LICENSE](LICENSE.txt) file.