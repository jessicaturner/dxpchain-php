<?php

namespace Codaone\DxpChain\Component;

use Codaone\DxpChain\DxpChain;
use Codaone\DxpChain\Component\Base\DataClass;

/**
 * Class Asset
 * @package Codaone\DxpChain\Component
 *
 * @method getId
 * @method string getSymbol
 * @method string|integer getPrecision
 */
class Asset extends DataClass
{
    public function __construct($asset)
    {
        $cache = $this->getCacheData('asset', $asset);
        if (!$cache) {
            $dxpChain = new DxpChain();
            if (!count(explode('.', $asset)) == 3) {
                $asset = $dxpChain->getAssetIdFromString($asset);
            }
            $assetData = $dxpChain->getAssets([$asset]);
            $this->setData($assetData[0]);
            // cache with id and symbol
            $this->setCacheData('asset', $this->getId(), $assetData[0]);
            $this->setCacheData('asset', $this->getSymbol(), $assetData[0]);
        } else {
            $this->setData($cache);
        }
    }

    /**
     * @return Account
     */
    public function getIssuer()
    {
        $issuerId = $this->getData('issuer');
        $issuer   = new Account($issuerId);
        return $issuer;
    }

    /**
     * @example DXP USD OPEN.BTC
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getSymbol();
    }
}