<?php require('vendor/autoload.php');
include('src/DxpChain.php');
include('src/Component/Asset.php');
include('src/Component/Market.php');

use Codaone\DxpChain\DxpChain;
use Codaone\DxpChain\Component\Asset;
use Codaone\DxpChain\Component\Market;

$dxpChain = new DxpChain('wss://node.gvxlive.com');
$block = $dxpChain->getBlock('1742199');
print_r($block);
echo "<br>";
echo $dxpChain->getChainId();
echo "<br>";

echo "<br>";

//$block = $dxpChain->call('history', 'method_name', ['param1', 'param2']);

$asset = new Asset('DXP');
echo $asset->getId(); // 1.3.0
echo "<br>";
echo $asset->getPrecision(); // 5
echo "<br>Market : ";

$market = new Market('DXP/USD'); // delimiter can also be : _ -
echo $market->getVolume24h('DXP')->getAmount();
echo "<br>";
echo $market->getTicker();
echo "<br>";
echo $market->getOrderBook(25)->getAsks();
?>